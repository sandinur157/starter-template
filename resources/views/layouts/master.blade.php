<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title') - BS Guvilli System</title>
    
    <link rel="icon" href="{{ asset('public/adminlte/dist/img/logo1.png') }}">
    <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css">
    <!-- Plugins -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/overlayScrollbars/css/OverlayScrollbars.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/datatables/dataTables.bootstrap4.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/buttons.dataTables.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/datepicker/datepicker3.css') }}">
    <!-- Tempusdominus Bbootstrap 4 -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/tempusdominus-bootstrap-4/css/tempusdominus-bootstrap-4.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/css/sweetalert2.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/select2/css/select2.min.css') }}">
    <!-- fullCalendar -->
    <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/fullcalendar/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/fullcalendar-daygrid/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/fullcalendar-timegrid/main.min.css') }}">
    <link rel="stylesheet" href="{{ asset('public/adminlte/plugins/fullcalendar-bootstrap/main.min.css') }}">
    <!-- Theme style -->
    <link rel="stylesheet" href="{{ asset('public//adminlte/dist/css/adminlte.css') }}">
    <!-- Google Font: Source Sans Pro -->
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700" rel="stylesheet">
    <style>
        .dt-buttons .dt-button {
            display: inline-block;
            font-weight: 400;
            text-align: center;
            white-space: nowrap;
            vertical-align: middle;
            -webkit-user-select: none;
            -moz-user-select: none;
            -ms-user-select: none;
            user-select: none;
            border: 1px solid transparent;
            padding: 0.375rem 0.75rem;
            font-size: 1rem;
            line-height: 1.5;
            border-radius: 0.25rem;
            transition: color 0.15s ease-in-out, background-color 0.15s ease-in-out, border-color 0.15s ease-in-out, box-shadow 0.15s ease-in-out;
            padding: 0.25rem 0.5rem;
            font-size: 0.875rem;
            line-height: 1.5;
            border-radius: 0.2rem;
            color: #fff;
            background: #007bff;
            border-color: #007bff;
        }

        button.dt-button.buttons-collection.buttons-page-length:hover, 
        button.dt-button.buttons-excel.buttons-html5:hover
        {
            color: #fff;
            background: #0069d9;
            border-color: #0062cc;
        }

        button.dt-button.buttons-collection.buttons-page-length:focus, 
        button.dt-button.buttons-excel.buttons-html5:focus
        {
            box-shadow: 0 0 0 0.2rem rgba(0, 123, 255, 0.5);
            background: #007bff;
        }

        .btn-outline-secondary {
            background-color: #fff;
        }
    </style>
    @stack('css')
</head>
<body class="hold-transition sidebar-mini">
    <div class="wrapper">
        
        @include('layouts.partials.nav')
        
        @include('layouts.partials.sidebar')
        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
            <!-- Content Header (Page header) -->
            <div class="content-header">
                <div class="container-fluid">
                    <div class="row mb-2">
                        <div class="col-sm-6">
                            <h1 class="m-0 text-dark">
                                @yield('title')
                            </h1>
                        </div>

                        <div class="col-sm-6">
                            <ol class="breadcrumb float-sm-right">
                            @section('breadcrumb')
                                <li class="breadcrumb-item"><a href="{{ url('/') }}">Home</a></li>
                            @show
                            </ol>
                        </div>
                    </div>
                </div>
            </div>
            <!-- /.content-header -->
            <!-- Main content -->
            <div class="content">
                <div class="container-fluid">
                    
                    @yield('main-content')
                    
                </div>
            </div>
            <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->
        @include('layouts.partials.footer')
    </div>
    <!-- ./wrapper -->

    <!-- REQUIRED SCRIPTS -->
    <!-- jQuery -->
    <script src="{{ asset('public/adminlte/plugins/jquery/jquery.min.js') }}"></script>
    <script src="{{ asset('public/adminlte/plugins/jquery-ui/jquery-ui.min.js') }}"></script>
    <!-- Bootstrap 4 -->
    <script src="{{ asset('public/adminlte/plugins/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
    <!-- Plugins -->
    <script src="{{ asset('public/adminlte/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js') }}"></script>
    <script src="{{ asset('public/adminlte/plugins/fastclick/fastclick.js') }}"></script>
    <script src="{{ asset('public/adminlte/plugins/datatables/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('public/adminlte/plugins/datatables/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('public/adminlte/plugins/datepicker/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('public/js/sweetalert2.min.js') }}"></script>
    <script src="{{ asset('public/adminlte/plugins/moment/moment.min.js') }}"></script>
    <script src="{{ asset('public/adminlte/plugins/select2/js/select2.full.min.js') }}"></script>
    <script src="{{ asset('public/adminlte/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js') }}"></script>
    <script src="{{ asset('public/js/validator.min.js') }}"></script>
    <script src="{{ asset('public/js/simple-money-format.js') }}"></script>
    <!-- AdminLTE App -->
    <script src="{{ asset('public/adminlte/dist/js/adminlte.min.js') }}"></script>
    <script>
        jQuery(() => {
            $.widget.bridge('uibutton', $.ui.button)

            $('.custom-file-input').on('change', function() {
                let fileName = $(this).val().split('\\').pop()
                $(this).next('.custom-file-label').addClass('selected').html(fileName)
            })

            $('.nav-sidebar a').filter(function() {
                return this.href == window.location.href;
            }).addClass('active');

            $('.nav-sidebar a').filter(function() {
                return this.href +'#' == window.location.href;
            }).addClass('active');

            $('.menu-master .nav a').filter(function() {
                return this.href == window.location.href;
            }).parent().parent().prev().addClass('active').parent().addClass('menu-open');

            $('.menu-master .nav a').filter(function() {
                return this.href+'*' == window.location.href;
            }).parent().parent().prev().addClass('active').parent().addClass('menu-open');

            $('.menu-master .nav a').filter(function() {
                return this.href +'#' == window.location.href;
            }).parent().parent().prev().addClass('active').parent().addClass('menu-open');

            $('.money').simpleMoneyFormat();
            $('.money').on('keypress', function (e) {
                let charCode = (e.which) ? e.which : event.keyCode
                if (charCode > 31 && (charCode < 48 || charCode > 57))
                    return false;
                return true;
            })
        })

    </script>
    @stack('scripts')
</body>
</html>