<!-- Main Footer -->
<footer class="main-footer">
	<!-- To the right -->
	<div class="float-right d-none d-sm-inline">
		Created with <i class="fas fa-heart text-danger"></i> by Nursandi
	</div>
	<!-- Default to the left -->
	<strong>Copyright &copy; 2019 <a href="https://adminlte.io">Alfaris Solution ID</a>.</strong> All rights reserved.
</footer>