<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use Auth;
use Hash;
use Image;
use Storage;
use Carbon\Carbon;

class UserController extends Controller
{
    public function index()
    {
        return view('users.index');
    }

    
    public function listData() 
    {
        $user = User::where('level', '!=', 1)->orderBy('id', 'desc')->get();
        $no = 0;
        $data = array();

        foreach ($user as $list) {
            $no++;
            $row = array();
            $row[] = $no;
            $row[] = $list->name;
            $row[] = $list->username;
            $row[] = $list->email;
            $row[] = '
                    <a href="#" onclick="edit('. $list->id .')" class="btn btn-primary btn-sm float-left mx-1"><i class="fas fa-edit"></i></a>
                    <a href="#" onclick="_delete('. $list->id .')" class="btn btn-danger btn-sm float-left"><i class="fas fa-trash"></i></a>
            ';
            $data[] = $row;
        }

        $output = ['data' => $data];
        return response()->json($output);
    }

    
    public function store(Request $request)
    {
        $user = new User;
        $user->name = $request['nama'];
        $user->username = $request['username'];
        $user->email = $request['email'];
        $user->password = bcrypt($request['password']);
        $user->level = 2;
        $user->foto = 'user.png';
        $user->save();

        return response()->json([
            'message' => 'Data Added Successfully'
        ]);
    }

    
    public function edit($id)
    {
        $user = User::find($id);
        echo json_encode($user);
    }

    
    public function update(Request $request, $id)
    {
        $user = User::find($id);
        $user->name = $request['nama'];
        $user->username = $request['username'];
        $user->email = $request['email'];
        if(!empty($request['password'])) $user->password = bcrypt($request['password']);
        $user->update();

        return response()->json([
            'message' => 'Data Added Successfully'
        ]);
    }

    
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();

        return response()->json([
            'message' => 'Data Added Successfully'
        ]);
    }

    public function _saveFile($name, $image)
    {
        $name = str_slug($name).'-'. Carbon::now()->toDateTimeString() .'.'. $image->getClientOriginalExtension();

        if (!Storage::disk('public')->exists('uploads/users')) {
            Storage::disk('public')->makeDirectory('uploads/users');
        }
        
        $image = Image::make($image)->resize(400, 400)->save();
        Storage::disk('public')->put('uploads/users/'. $name, $image);
        
        return $name;
    }


    public function profile() 
    {
    	$users = Auth::user();
    	return view('users.profile', compact('users'));
    }


    public function changeProfile(Request $request, $id) 
    {
    	$user = User::find($id);
        $user->name = $request->nama;

    	if(!empty($request['password'])) {
    		if(Hash::check($request['passwordlama'], $user->password)) {
    			$user->password = bcrypt($request['password']);
    		}
    	}

    	if($request->hasFile('foto')) {
    		$image = $this->_saveFile($request->nama, $request->file('foto'));

            isset($image) ? Storage::disk('public')->delete('uploads/users/'. $user->foto) : '';
    	} else $image = $user->foto;

        $user->foto = $image;
        $user->update();

        return response()->json([
            'name' => $user->name,
            'image' => $image,
            'message' => 'Data Updated Successfully'
        ]);
    }
}
