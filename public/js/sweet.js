function _swall(message) {
    let Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 1000,
        type: 'success',
        title: 'message'
    });

    Toast.fire({
        type: 'success',
        title: message
    })
}